"use strict";

let game = {

  // properties
  width: 0,
  height: 0,
  currentStep: 0,
  mount: "#app",
  app: null,
  stepForwardButton: null,
  stepCounter: null,
  database: [],

  // methods

  init(width = 10, height = 10, mount = "#app", stepForwardButtonId = "#next-step", stepCounterId = "#counter", random = true) {
    console.log("Init Game"); // debug
    this.width = width;
    this.height = height;
    this.currentStep = 0;
    this.mount = mount;
    this.app = document.querySelector(mount);
    this.stepForwardButton = document.querySelector(stepForwardButtonId);
    this.stepForwardButton.addEventListener('click', () => {
      this.evolution();
      this.drawGame();
    });
    this.stepCounter = document.querySelector(stepCounterId);
    this.generateCleanDatabase();
    this.generateSeed(random);
    document.addEventListener('keypress', (evt) => {
      if (evt.key === " ") {
        this.evolution();
        this.drawGame();
      }
    });

  },


  generateCleanDatabase() {
    console.log("Generating Clean Database"); // debug
    this.database = new Array(this.height);
    for (let row = 0; row < this.height; row++) {
      this.database[row] = new Array(this.width);
    }
  },

  generateSeed(random = true) {
    console.log("Generating Seed");  // debug
    for (let row = 0; row < this.height; row++) {
      for (let column = 0; column < this.width; column++) {
        this.database[row][column] = false;
        if (random) {
          this.database[row][column] = Math.random() > 0.5;
        }
      }
    }
  },

  drawGame() {
    console.log(`Draw Game Step ${this.currentStep}`);  // debug
    let fragment = new DocumentFragment();
    for (let row = 0; row < this.height; row++) {

      let rowElement = document.createElement('div');
      rowElement.classList.add('row');
      fragment.append(rowElement);

      for (let cell = 0; cell < this.width; cell++) {

        let cellElement = document.createElement('div');
        cellElement.classList.add('cell');
        rowElement.append(cellElement);

        let dotElement = document.createElement('div');
        const state = this.database[row][cell] ? 'alive' : 'dead';
        dotElement.classList.add('dot');
        dotElement.setAttribute('data-state', state);
        cellElement.append(dotElement);
      }
    }
    this.app.innerHTML = "";
    this.app.append(fragment);
    this.stepCounter.innerHTML = "Step #" + this.currentStep;
  },

  evolution() {

    console.log(`Evolution Step ${this.currentStep + 1}`);  // debug

    let newGeneration = [...this.database].map(row => [...row]);

    for (let row = 0; row < this.height; row++) {
      for (let column = 0; column < this.width; column++) {

        const nextRow = row === this.height - 1 ? 0 : row + 1;
        const nextColumn = column === this.width - 1 ? 0 : column + 1;

        const previousRow = row === 0 ? this.height - 1 : row - 1;
        const previousColumn = column === 0 ? this.width - 1 : column - 1;

        const neighboursCount =
          this.database[previousRow][previousColumn] +
          this.database[previousRow][column] +
          this.database[previousRow][nextColumn] +
          this.database[row][previousColumn] +
          this.database[row][nextColumn] +
          this.database[nextRow][previousColumn] +
          this.database[nextRow][column] +
          this.database[nextRow][nextColumn];

        if (this.database[row][column]) {
          newGeneration[row][column] = !(neighboursCount < 2 || neighboursCount > 3);
        } else {
          newGeneration[row][column] = neighboursCount === 3;
        }
      }
    }
    this.database = newGeneration;
    this.currentStep++;
  }
};

game.init(130, 80);
game.drawGame();